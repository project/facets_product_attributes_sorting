INTRODUCTION
------------

This module allows you to sort the facet filters of the attributes of product variations by their weight (the one that has been configured in the list of attributes).

By default, commerce + facets only allow to sort the filters by alphabetical order of the machine or label name. If you are looking for a better way to sort product attribute filters, this is your module.

This was the requirement of a project in which they had to order the sizes of the clothes (the products were clothing) in the order that has been configured (the same order used in the forms of "add to cart" ) and not in alphabetical order.

REQUIREMENTS
------------

This module requires the following modules:
* Commece (https://www.drupal.org/project/commerce)
* Facets (https://www.drupal.org/project/facets)

INSTALLATION
------------

* Install as you would normally install a Drupal module contributed.

CONFIGURATION
-------------

* Select "Sort by product attributes weight" in "Facet sorting" configuration in the facets config filter.
